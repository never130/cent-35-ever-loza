
public class FooClase {
	// Atributo privado. Solo puedes acceder dentro
	// de la misma clase.
	private String s1 = "private string";

	// Atributo protegido. Puedes acceder dentro de la misma clase, clases hijas
	// (herencia)
	// y de las clases del mismo paquete (packages).
	protected String s2 = "protected string";

	// Atributo publico. Puede ser accedido en cualquier lugar
	// No se recomienda ya que rompe el principio de ocultamiento.
	public String s3 = "public string";

	// Atributo Default o por defecto. Puedes acceder dentro de la misma clase
	// y de las clases del mismo paquete (packages)
	String s4 = "string whitohut acess modifier";

	// Metodo privado. Solo puedes acceder dentro
	// de la misma clase.
	private void foo() {
	}

	// Metodo Protegido. Puedes acceder dentro de la misma clase, clases hijas
	// (herencia)
	// y de las clases del mismo paquete (packages)
	protected void var() {
	}

	// Metodo publico. Puede ser accedido en cualquier lugar
	// No se recomienda ya que rompe el principio de ocultamiento.
	public void baz() {
	}

	// Metodo Defaul o por defecto. Puedes acceder dentro de la misma clase
	// y de las clases del mismo paquete (packages)
	void fooBar() {
	}

}
