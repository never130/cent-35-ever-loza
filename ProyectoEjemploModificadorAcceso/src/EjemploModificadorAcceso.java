
public class EjemploModificadorAcceso {

	public static void main(String[] args) {

		FooClase t = new FooClase();
		// Error de compilacion esperado
		// Tratando accediendo a un atributo privado de la clase FooClase
		System.out.println("s1 = " + t.s1);

		// Correcto, no hay errores de compilacion
		// accediendo a un atributo protegido
		System.out.println("s2 = " + t.s2);

		// Accediendo a un atributo public
		System.out.println("s3 = " + t.s3);

		// Accediendo a un atributo default.
		System.out.println("s4 = " + t.s4);

		// Error de compilacion esperado
		// Tratando accediendo a un metodo privado de la clase FooClase
		t.foo();

		// Correcto, no hay errores de compilacion
		t.bar(); // llamando a un metodo protegido
		t.baz(); // llamando a un metodo publico
		t.fooBar(); // llamando a un metodo default

	}

}
