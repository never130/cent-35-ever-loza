
public class TestTiposDatos {

	public static void main(String[] args) {
		Thread objetoHilo = new Thread("Algun Proceso Hilo");
		String stringObjeto = "Java Standard Edition";
		char[] charArreglo = {'a','b','c'};
		int integerPrimitivo = 4;
		long longPrimitivo = Long.MIN_VALUE;
		float floatPrimitivo = Float.MAX_VALUE;
		double doublePrimitivo = Math.PI;
		boolean booleanPrimitivo = true;
		
		System.out.println(objetoHilo);
		System.out.println(stringObjeto);
		System.out.println(charArreglo);
		System.out.println(integerPrimitivo);
		System.out.println(longPrimitivo);
		System.out.println(floatPrimitivo);
		System.out.println(doublePrimitivo);
		System.out.println(booleanPrimitivo);
		
	}

}
