
public class ClaseBoolean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    boolean booleanVar = 1>2;
    Boolean booleanObj = new Boolean ("true");
    
    /* Converir de primtivo a objeto Boolean, tambi�n se puede
     * usar el metodo valueOf */
    Boolean booleanObj2 = new Boolean(booleanVar);
    
    System.out.println("booleanVar = " + booleanVar);
    System.out.println("booleanObj = " + booleanObj);
    System.out.println("booleanObj2 = " + booleanObj2);
    System.out.println("comparando 2 objectos Boolean: " + booleanObj.equals(booleanObj2));
    
    /* Convertir de Objeto Boolean a primitivo*/
    booleanVar = booleanObj.booleanValue();
    System.out.println("booleanVar = " + booleanVar);   
     
	}

}
