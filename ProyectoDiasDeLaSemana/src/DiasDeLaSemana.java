
public class DiasDeLaSemana {

	public static void main(String[] args) {
		String[] dias = new String[7];
		dias[0] = "Lunes";
		dias[1] = "Martes";
		dias[2] = "Miercoles";
		dias[3] = "Jueves";
		dias[4] = "Viernes";
		dias[5] = "Sabado";
		dias[6] = "Domingo";

		System.out.println("Imprimimos los dias de la semana con un loop while");
		int i = 0;
		while (i < dias.length) {
			System.out.println(dias[i]);
			i++;
		}

		System.out.println("Imprimimos los dais de la semana con un do-while");
		i = 0;
		do {
			System.out.println(dias[i]);
			i++;
		} while (i < dias.length);

		System.out.println("Imprimimos los dias de la semana con un loop for");
		for (int j = 0; j < dias.length; j++) {

			System.out.println(dias[j]);
		}

	}

}
