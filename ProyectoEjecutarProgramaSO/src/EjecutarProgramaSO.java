
public class EjecutarProgramaSO {

	public static void main(String[] args) {
		Runtime rt = Runtime.getRuntime();
		Process proc;
		
		try {
		
			if (System.getProperty("os.name").startsWith("Windows")){
				// Ejecutar un Programa del Sistema Operativo
				proc = rt.exec("notepad");
			}
			
			else{
				proc = rt.exec("gedit");
			}
			
			proc.waitFor();
			
		    } catch (Exception e){
			System.out.println("notepad es un comando desconocido.");
		    }

	}

}
