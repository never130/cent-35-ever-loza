package clase2;

public class EjemploDouble {

	public static void main(String[] args) {
		
		
		System.out.println(Double.MAX_VALUE);	
		
		Double d = Double.valueOf("3.14");
		String s = Double.toString(d);
		System.out.println(s);
		
		boolean b = Double.isFinite(d);
		System.out.println(b);
		d = new Double (Math.PI);
		
		String o = d.toString();
		System.out.println(o);
		int i = d.intValue();
		System.out.println(i);
		long l = d.longValue();
		System.out.println(l);
		float d2 = d.floatValue();
		System.out.println(d2);
		double d3 = d.doubleValue();
		System.out.println(d3);
		Double d4 = Double.parseDouble("3.1415927");
	    System.out.println(d4);
	}

}
