import Edu.EjemploClase.Escuela.ProfesorInterino;
import Edu.EjemploClase.Escuela.ProfesorSuplente;
import Edu.EjemploClase.Escuela.ProfesorTitular;

public class Principal {

	public static void main(String[] args) {
		ProfesorInterino profesor1 = new ProfesorInterino();
		ProfesorSuplente profesor2 = new ProfesorSuplente();
		ProfesorTitular profesor3 = new ProfesorTitular();
		
		
		profesor1.setNombreApellido("Martin Mirabete");
		profesor2.setNombreApellido("Rafael Yanez");
		profesor3.setNombreApellido("Chauque");
		
		System.out.println(profesor1);
		System.out.println(profesor2);
		System.out.println(profesor3);
	}

}
