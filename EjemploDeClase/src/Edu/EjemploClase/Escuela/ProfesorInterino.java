package Edu.EjemploClase.Escuela;

public class ProfesorInterino extends Profesor {
	private String nombreApellido;
	int Edad;

	public static void main(String[] args) {

	}

	public String getNombreApellido() {
		return nombreApellido;
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	public int getEdad() {
		return Edad;
	}

	public void setEdad(int edad) {
		this.Edad = edad;
	}

	@Override
	public String toString() {
		return "Profesor Interino Nombre y Apellido: " + this.getNombreApellido();

	}
}
