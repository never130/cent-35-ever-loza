import java.security.GeneralSecurityException;

public class Estudiante {
	// Declaramos las variables de instancia.
	private String nombre;
	private String apellido;
	private double notaMatematica;
	private double notaLenguaje;
	private double notaCiencia;
	// Declaramos las variables estaticas o de la clase.
	private static int contadorAlumnos = 0;

	/**
	 * Calcula el promedio de las asignaturas, matematicas, lenguaje y ciencias
	 */

	public double getPromedio() {
		double resultado = 0;
		resultado = (getNotaMatematica() + getNotaLenguaje() + getNotaCiencia()) / 3;
		return resultado;
	}

	/**
	 * Retorna el numero de las instancias/objetos de la clase Estudiante
	 * 
	 */

	public static int getContadorAlumnos() {
		return contadorAlumnos;
	}

	/**
	 * Incrementa el numero de las instancias/objetos de la clase Estudiante.
	 * Este es un metodo estatico.
	 */

	public static void incrementaContadorAlumnos() {
		contadorAlumnos++;

	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getNotaLenguaje() {
		return notaLenguaje;
	}

	public void setNotaLenguaje(double notaLenguaje) {
		this.notaLenguaje = notaLenguaje;
	}

	public double getNotaMatematica() {
		return notaMatematica;
	}

	public void setNotaMatematica(double notaMatematica) {
		this.notaMatematica = notaMatematica;
	}

	public double getNotaCiencia() {
		return notaCiencia;
	}

	public void setNotaCiencia(double notaCiencia) {
		this.notaCiencia = notaCiencia;
	}
}
