
public class EjemploEstudiante {

	public static void main(String[] args) {

		// Creamos un objeto instancia de la clase Estudiante.
		Estudiante andres = new Estudiante();

		// Incrementamos el contador de alumnos llamando al metodo estatico.
		Estudiante.incrementaContadorAlumnos();

		// Creamos otro objeto instancia de la clase Estudiante.
		Estudiante james = new Estudiante();

		// Incrementamos el contador de alumnos llamando al metodo estatico.
		Estudiante.incrementaContadorAlumnos();

		// Creamos un tercer objeto instancia de la clase Estudiante.
		Estudiante john = new Estudiante();

		// Incrementamos el contador de alumnos llamando al metodo estatico.
		Estudiante.incrementaContadorAlumnos();

		// Asignamos los nombres y apellidos de los estudiantes.
		andres.setNombre("Andres");
		andres.setApellido("Guzman");
		james.setNombre("James");
		james.setApellido("Gosling");
		john.setNombre("John");
		john.setApellido("Doe");

		// Imprimimos el nombre y apellido de Andres.
		System.out.println("Nombres = " + andres.getNombre() + " " + andres.getApellido());

		// Imprimimos el numero de estudiantes.
		System.out.println("Total de Estudiantes = " + Estudiante.getContadorAlumnos());
		// Asignamos las notas del alumno John Doe.
		john.setNotaCiencia(5.9);
		john.setNotaLenguaje(6.3);
		john.setNotaMatematica(6.8);
		
		// Imprimimos el promedio de notas de John Doe.
		System.out.println("Promedio de John Doe =" + john.getPromedio());
	}

}
