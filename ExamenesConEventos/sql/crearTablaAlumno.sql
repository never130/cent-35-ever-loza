CREATE TABLE Alumno(
   dni int NOT NULL,
   primerNombre varchar (30) NOT NULL,
   segundoNombre varchar (30) NOT NULL,
   apellido varchar (30) NOT NULL,
   email varchar (30) NOT NULL,
   telefono varchar (11) NOT NULL,
   codigoCarrera int NOT NULL,
   estado varchar (12) NOT NULL,
   PRIMARY KEY (dni)
);
