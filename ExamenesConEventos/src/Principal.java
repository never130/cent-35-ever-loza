
import java.awt.FlowLayout;

import javax.swing.JFrame;

import edu.cent35.examenes.alumno.AlumnoUI;

/**
 * Clase aut�noma para visualizar la ventana
 * 
 * @author Ever Loza, B�rbara Hern�ndez
 *
 */
public class Principal {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER));
		frame.getContentPane().add(new AlumnoUI());
		frame.setSize(670, 320);
		frame.setVisible(true);
		frame.setTitle("CRUD Alumno");				
	}

}

