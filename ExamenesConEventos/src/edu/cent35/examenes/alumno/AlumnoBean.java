package edu.cent35.examenes.alumno;

import java.sql.SQLException;
import javax.sql.rowset.JdbcRowSet;
import javax.swing.JOptionPane;

import com.sun.rowset.JdbcRowSetImpl;

/**
 * Funcionalidad para interactuar con la tabla alumno
 * 
 * @author Ever Loza, B�rbara Hern�ndez
 *
 */

public class AlumnoBean {

	static final String JDBC_DRIVER = "org.firebirdsql.jdbc.FBDriver";
	static final String DB_URL = "jdbc:firebirdsql:localhost/3050:C:\\DBPP\\EXAMENES.FDB";
	static final String DB_USER = "SYSDBA";
	static final String DB_PASS = "masterkey";

	private JdbcRowSet tabla = null;

	// Constructor de la clase
	public AlumnoBean() {

		try {
			Class.forName(JDBC_DRIVER);
			tabla = new JdbcRowSetImpl();
			tabla.setUrl(DB_URL);
			tabla.setUsername(DB_USER);
			tabla.setPassword(DB_PASS);
			tabla.setCommand("SELECT * FROM Alumno");
			tabla.execute();
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * @param alumno
	 * @return La instancia de alumno creada en la tabla
	 */
	public Alumno crear(Alumno alumno) {
		try {
			tabla.moveToInsertRow();
			tabla.updateInt("dni", alumno.getDni());
			tabla.updateString("primerNombre", alumno.getPrimerNombre());
			tabla.updateString("segundoNombre", alumno.getSegundoNombre());
			tabla.updateString("apellido", alumno.getApellido());
			tabla.updateString("email", alumno.getEmail());
			tabla.updateString("telefono", alumno.getTelefono());
			tabla.updateInt("codigoCarrera", alumno.getCodigoCarrera());
			tabla.updateString("estado", alumno.getEstado());
			// Inserta un registro en la tabla
			tabla.insertRow();
			// Mueve al registro actual
			tabla.moveToCurrentRow();
		} catch (SQLException ex) {
			try {
				tabla.rollback();
				alumno = null;
			} catch (SQLException e) {

			}

		}
		return alumno;
	}

	/**
	 * @param alumno
	 * @return La instancia del alumno con los datos actualizados en la BD
	 */
	public Alumno actualizar(Alumno alumno) {
		try {
			tabla.updateString("primerNombre", alumno.getPrimerNombre());
			tabla.updateString("segundoNombre", alumno.getSegundoNombre());
			tabla.updateString("apellido", alumno.getApellido());
			tabla.updateString("email", alumno.getEmail());
			tabla.updateString("telefono", alumno.getTelefono());
			tabla.updateInt("codigoCarrera", alumno.getCodigoCarrera());
			tabla.updateString("estado", alumno.getEstado());
			// Actualiza el registro en la Tabla
			tabla.updateRow();
			// Mueve al registro actual
			tabla.moveToCurrentRow();
		} catch (SQLException ex) {
			try {
				tabla.rollback();
			} catch (SQLException e) {

			}
			ex.printStackTrace();
		}
		return alumno;
	}

	/**
	 * Elimina el registro actual de la Tabla
	 */
	public void borrar() {
		try {
			// Mueve al registro actual
			tabla.moveToCurrentRow();
			// Elimina el registro de la BD
			tabla.deleteRow();
		} catch (SQLException ex) {
			try {
				tabla.rollback();
			} catch (SQLException e) {
			}
			ex.printStackTrace();
		}

	}

	/**
	 * @return Instancia de alumnos con los datos del registro
	 */
	public Alumno mueveAlPrimero() {
		Alumno alumno = new Alumno();
		try {
			if (tabla.getRow() != 0) {
				tabla.first();
				alumno.setDni(tabla.getInt("dni"));
				alumno.setPrimerNombre(tabla.getString("primerNombre"));
				alumno.setSegundoNombre(tabla.getString("segundoNombre"));
				alumno.setApellido(tabla.getString("apellido"));
				alumno.setEmail(tabla.getString("email"));
				alumno.setTelefono(tabla.getString("telefono"));
				alumno.setCodigoCarrera(tabla.getInt("codigoCarrera"));
				alumno.setEstado(tabla.getString("estado"));

			} else {
				JOptionPane.showMessageDialog(null, "No hay registros");
			}
		

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return alumno;
	}

	/**
	 * @return Instancia de alumnos con los datos del registro
	 */
	public Alumno mueveAlUltimo() {
		Alumno alumno = new Alumno();
		try {
			if (tabla.getRow() != 0) {
				tabla.last();
				alumno.setDni(tabla.getInt("dni"));
				alumno.setPrimerNombre(tabla.getString("primerNombre"));
				alumno.setSegundoNombre(tabla.getString("segundoNombre"));
				alumno.setApellido(tabla.getString("apellido"));
				alumno.setEmail(tabla.getString("email"));
				alumno.setTelefono(tabla.getString("telefono"));
				alumno.setCodigoCarrera(tabla.getInt("codigoCarrera"));
				alumno.setEstado(tabla.getString("estado"));
			} else {
				JOptionPane.showMessageDialog(null, "No hay registros");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return alumno;

	}

	/**
	 * @return Instancia de alumnos con los datos del registro
	 */
	public Alumno mueveAlProximo() {
		Alumno alumno = new Alumno();
		try {
			if (tabla.getRow() != 0) {
				if (tabla.next() == false)
					tabla.previous();
				alumno.setDni(tabla.getInt("dni"));
				alumno.setPrimerNombre(tabla.getString("primerNombre"));
				alumno.setSegundoNombre(tabla.getString("segundoNombre"));
				alumno.setApellido(tabla.getString("apellido"));
				alumno.setEmail(tabla.getString("email"));
				alumno.setTelefono(tabla.getString("telefono"));
				alumno.setCodigoCarrera(tabla.getInt("codigoCarrera"));
				alumno.setEstado(tabla.getString("estado"));
			} else {
				JOptionPane.showMessageDialog(null, "No hay registros");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return alumno;
	}

	/**
	 * @return Instancia de alumnos con los datos del registro
	 */
	public Alumno mueveAlAnterior() {
		Alumno alumno = new Alumno();
		try {
			if (tabla.getRow() != 0) {
				if (tabla.previous() == false)
					tabla.next();
				alumno.setDni(tabla.getInt("dni"));
				alumno.setPrimerNombre(tabla.getString("primerNombre"));
				alumno.setSegundoNombre(tabla.getString("segundoNombre"));
				alumno.setApellido(tabla.getString("apellido"));
				alumno.setEmail(tabla.getString("email"));
				alumno.setTelefono(tabla.getString("telefono"));
				alumno.setCodigoCarrera(tabla.getInt("codigoCarrera"));
				alumno.setEstado(tabla.getString("estado"));
			} else {
				JOptionPane.showMessageDialog(null, "No hay registros");
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return alumno;
	}

	/**
	 * @return Instancia de alumnos con los datos del registro
	 */
	public Alumno getDatosRegistro() {
		Alumno alumno = new Alumno();
		try {
			if (tabla.getRow() != 0) {
				tabla.moveToCurrentRow();
				alumno.setDni(tabla.getInt("dni"));
				alumno.setPrimerNombre(tabla.getString("primerNombre"));
				alumno.setSegundoNombre(tabla.getString("segundoNombre"));
				alumno.setApellido(tabla.getString("apellido"));
				alumno.setEmail(tabla.getString("email"));
				alumno.setTelefono(tabla.getString("telefono"));
				alumno.setCodigoCarrera(tabla.getInt("codigoCarrera"));
				alumno.setEstado(tabla.getString("estado"));
			} else {
				JOptionPane.showMessageDialog(null, "No hay registros");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return alumno;
	}

}
