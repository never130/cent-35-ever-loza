package edu.cent35.examenes.alumno;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import net.miginfocom.swing.*;

/**
 * Panel que contiene los detalles de alumno y los botones de acciones CRUD
 * 
 * @author Ever Loza, B�rbara Hern�ndez
 *
 */
public class AlumnoUI extends JPanel {
	private static final long serialVersionUID = 1L;

	// Instancias de cuadros de texto para cada uno de los campos
	private JTextField txtDni = new JTextField(8);
	private JTextField txtPrimerNombre = new JTextField(30);
	private JTextField txtSegundoNombre = new JTextField(30);
	private JTextField txtApellido = new JTextField(30);
	private JTextField txtEmail = new JTextField(30);
	private JTextField txtTelefono = new JTextField(11);
	private JTextField txtCodigoCarrera = new JTextField(4);
	private JTextField txtEstado = new JTextField(12);

	// Se crean las instancias para los botones
	private JButton btnNuevo = new JButton("Nuevo...");
	private JButton btnActualizar = new JButton("Actualizar");
	private JButton btnBorrar = new JButton("Borrar");
	private JButton btnPrimero = new JButton("Primero");
	private JButton btnAnterior = new JButton("Anterior");
	private JButton btnProximo = new JButton("Pr�ximo");
	private JButton btnUltimo = new JButton("Ultimo");

	private AlumnoBean bean = new AlumnoBean();

	/**
	 * Constructor del panel
	 */
	public AlumnoUI() {
		// Tipo y la separaci�n del borde
		setLayout(new BorderLayout(5, 5));
		// T�tulo del recuadro
		setBorder(new TitledBorder(new EtchedBorder(), "Detalles del alumno"));
		// Agrega el panel de campos
		add(iniCampos(), BorderLayout.NORTH);
		// Agrega el panel de botones
		add(iniBotones(), BorderLayout.CENTER);
		setDatos(bean.mueveAlPrimero());
	}

	/**
	 * @return panel que contiene los botones
	 */
	private JPanel iniBotones() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 3, 3));
		panel.add(btnNuevo);
		btnNuevo.addActionListener(new ManejadorBotones());
		panel.add(btnActualizar);
		btnActualizar.addActionListener(new ManejadorBotones());
		panel.add(btnBorrar);
		btnBorrar.addActionListener(new ManejadorBotones());
		panel.add(btnPrimero);
		btnPrimero.addActionListener(new ManejadorBotones());
		panel.add(btnAnterior);
		btnAnterior.addActionListener(new ManejadorBotones());
		panel.add(btnProximo);
		btnProximo.addActionListener(new ManejadorBotones());
		panel.add(btnUltimo);
		btnUltimo.addActionListener(new ManejadorBotones());
		return panel;
	}

	/**
	 * @return panel que contiene los campos
	 */
	private JPanel iniCampos() {
		JPanel panel = new JPanel();
		/*
		 * Asigna el layout a utilizar
		 * http://thesuperjez.blogspot.com.ar/2009/05/hablemos-de-java-miglayout
		 * html http://www.miglayout.com/
		 */
		panel.setLayout(new MigLayout());

		// Agurega los campos y sus etiquetas
		panel.add(new JLabel("DNI:"), "align label");
		panel.add(txtDni, "wrap");
		// Hace que el campo sea de s�lo lectura
		txtDni.setEnabled(false);
		// Alinea junto a la etiqueta
		panel.add(new JLabel("Primer nombre:"), "align label");
		panel.add(txtPrimerNombre, "wrap"); // Realiza un salto de linea
		panel.add(new JLabel("Segundo nombre:"), "align label");
		panel.add(txtSegundoNombre, "wrap");
		panel.add(new JLabel("Apellido:"), "align label");
		panel.add(txtApellido, "wrap");
		panel.add(new JLabel("Email:"), "align label");
		panel.add(txtEmail, "wrap");
		panel.add(new JLabel("Tel�fono:"), "align label");
		panel.add(txtTelefono, "wrap");
		panel.add(new JLabel("C�digo de Carrera:"), "align label");
		panel.add(txtCodigoCarrera, "wrap");
		panel.add(new JLabel("Estado:"), "align label");
		panel.add(txtEstado, "wrap");
		return panel;
	}

	/**
	 * @return alumnos con los datos asignados desde los cuadros de texto
	 */
	private Alumno getDatos() {
		Alumno alumno = new Alumno();
		alumno.setDni(Integer.parseInt(txtDni.getText()));
		alumno.setPrimerNombre(txtPrimerNombre.getText());
		alumno.setSegundoNombre(txtSegundoNombre.getText());
		alumno.setApellido(txtApellido.getText());
		alumno.setEmail(txtEmail.getText());
		alumno.setTelefono(txtTelefono.getText());
		alumno.setCodigoCarrera(Integer.parseInt(txtCodigoCarrera.getText()));
		alumno.setEstado(txtEstado.getText());
		return alumno;
	}

	/**
	 * Carga los datos desde la instancia a los cuadros de texto
	 * 
	 * @param alumno
	 */
	private void setDatos(Alumno alumno) {
		txtDni.setText(String.valueOf(alumno.getDni()));
		txtPrimerNombre.setText(alumno.getPrimerNombre());
		txtSegundoNombre.setText(alumno.getSegundoNombre());
		txtApellido.setText(alumno.getApellido());
		txtEmail.setText(alumno.getEmail());
		txtTelefono.setText(alumno.getTelefono());
		txtCodigoCarrera.setText(String.valueOf(alumno.getCodigoCarrera()));
		txtEstado.setText(alumno.getEstado());
	}

	/**
	 * @return {@code True} si los datos est�n vac�os. En caso contrario
	 *         devolver� {@code False}
	 */
	private boolean isDatosVacios() {
		return (txtDni.getText().trim().isEmpty() && txtPrimerNombre.getText().trim().isEmpty() 
				&& txtSegundoNombre.getText().trim().isEmpty()
				&& txtApellido.getText().trim().isEmpty() && txtEmail.getText().trim().isEmpty()
				&& txtTelefono.getText().trim().isEmpty() && txtCodigoCarrera.getText().trim().isEmpty()
				&& txtEstado.getText().trim().isEmpty());
	}

	/**
	 * Clase para manejar los botones de la ventana
	 * 
	 * @author Ever Loza, B�rbara Hern�ndez
	 *
	 */
	private class ManejadorBotones implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Alumno alumno = getDatos();
			switch (e.getActionCommand()) {
			case "Guardar":
				// validacion edad a traves del dni
				if (alumno.getDni() <= 41000000) {
					
				} else {
					JOptionPane.showMessageDialog(null, "Edad menor, no se puede registrar");
					return;
				}
				if (isDatosVacios()) {
					JOptionPane.showMessageDialog(null, "No se puede crear un registro que esta vac�o.");
					return;
				}
				if (bean.crear(alumno) != null) 
					JOptionPane.showMessageDialog(null, "Nuevo alumno registrado correctamente.");

				txtDni.setEnabled(false);
				btnActualizar.setEnabled(true);
				btnBorrar.setEnabled(true);
				btnNuevo.setText("Nuevo...");
				break;
			case "Nuevo...":
				txtDni.setEnabled(true);
				btnActualizar.setEnabled(false);
				btnBorrar.setEnabled(false);
				alumno.setDni(0);
				alumno.setPrimerNombre("");
				alumno.setSegundoNombre("");
				alumno.setApellido("");
				alumno.setEmail("");
				alumno.setTelefono("");
				alumno.setCodigoCarrera(0);
				alumno.setEstado("");
				setDatos(alumno);
				btnNuevo.setText("Guardar");
				break;
			case "Actualizar":
				if (isDatosVacios()) {
					JOptionPane.showMessageDialog(null, "No se puede actualizar un registro vac�o.");
					return;
				}
				if (bean.actualizar(alumno) != null)
					JOptionPane.showMessageDialog(null, "El alumno con DNI:"
							+ String.valueOf(alumno.getDni() + " se ha actualizado correctamente"));
				break;
			case "Borrar":
				int result = JOptionPane.showConfirmDialog(null, " Estas seguro de continuar con la operacion?", null,
						JOptionPane.YES_NO_OPTION);
				if (isDatosVacios()) {
					JOptionPane.showMessageDialog(null, "No se puede eliminar un registro vac�o");
					return;
				}

				if (result == JOptionPane.NO_OPTION) {
					JOptionPane.showMessageDialog(null, "Acci�n cancelada");
				} else {
					alumno = bean.getDatosRegistro();
					bean.borrar();
					JOptionPane.showMessageDialog(null,
							"La persona con DNI:" + String.valueOf(alumno.getDni() + " se elimin� correctamente"));
					alumno.setDni(0);
					alumno.setPrimerNombre("");
					alumno.setSegundoNombre("");
					alumno.setApellido("");
					alumno.setEmail("");
					alumno.setTelefono("");
					alumno.setCodigoCarrera(0);
					alumno.setEstado("");
					setDatos(alumno);
				}
				break;
			case "Primero":
				txtDni.setEnabled(false);
				btnActualizar.setEnabled(true);
				btnBorrar.setEnabled(true);
				btnNuevo.setText("Nuevo...");
				setDatos(bean.mueveAlPrimero());
				JOptionPane.showMessageDialog(null, "Usted esta en el primer registro");
				break;
			case "Anterior":
				txtDni.setEnabled(false);
				btnActualizar.setEnabled(true);
				btnBorrar.setEnabled(true);
				btnNuevo.setText("Nuevo...");
				setDatos(bean.mueveAlAnterior());
				break;
			case "Pr�ximo":
				txtDni.setEnabled(false);
				btnActualizar.setEnabled(true);
				btnBorrar.setEnabled(true);
				btnNuevo.setText("Nuevo...");
				setDatos(bean.mueveAlProximo());
				break;
			case "Ultimo":
				txtDni.setEnabled(false);
				btnActualizar.setEnabled(true);
				btnBorrar.setEnabled(true);
				btnNuevo.setText("Nuevo...");
				setDatos(bean.mueveAlUltimo());
				JOptionPane.showMessageDialog(null, "Usted esta en el �ltimo registro");
				break;
			default:
				JOptionPane.showMessageDialog(null, "Comando inv�lido");
			}
		}
	}
}
