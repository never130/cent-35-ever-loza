
public class ClaseSystem {

	public static void main(String[] args) {
		long tiempoInicio, tiempoFin;
		
		int iteraciones = 10000;
		
		String concatenarNormal = "";
		
		/* inicializamos la concatenacion de una cadena en forma normal */
		
		tiempoInicio = System.currentTimeMillis();
		for (int i = 0; i < iteraciones; i++) {
			concatenarNormal += i;		
		}
        tiempoFin = System.currentTimeMillis();
        System.out.println("Tiempo trancurrido en la concatenacion de un String en forma normal: " + (tiempoFin-tiempoInicio) + " ms.");
        
        /* concatenando con StringBuilder ... /*
         * 
         */
        StringBuilder concatenarSB = new StringBuilder ();
        
        tiempoInicio = System.currentTimeMillis();
        for (int i = 0; i < iteraciones; i++) {
			concatenarSB.append(i);			
		}
        tiempoFin = System.currentTimeMillis();
        System.out.println("Tiempo transcurrido en la concatenacion con StringBuilder: " + (tiempoFin-tiempoInicio) + " ms.");
        //invocamos al garbage collector para trabajar en la recoleccion de basura (libera memoria)
        System.gc();
        System.exit(0);
	}

}
