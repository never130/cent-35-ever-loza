
public class Ave extends Animal{
	private boolean vuela;

	public Ave() {
		super();
	}
	
	public Ave(String nombre, int cantidadPatas, int edad, boolean vuela){
		super.setNombre(nombre);
		super.setCantidadPatas(cantidadPatas);
		super.setEdad(edad);
		this.vuela=vuela;
	}

	public boolean isVuela() {
		return vuela;
	}

	public void setVuela(boolean vuela) {
		this.vuela = vuela;
	}

}
