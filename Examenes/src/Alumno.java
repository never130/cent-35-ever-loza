
/**
 * POJO que representará una persona en el ámbito del sistema
 * 
 * @author Ever Loza, Bárbara Hernández
 *
 */
public class Alumno {

	private int dni;
	private String primerNombre;
	private String segundoNombre;
	private String apellido;
	private String email;
	private String telefono;
	private int codigoCarrera;
	private String estado;

	// Constructor de la clase sin campos
	public Alumno() {

	}

	// Constructor de la clase sin campos
	public Alumno(int dni, String primerNombre, String segundoNombre, String apellido, String email, String telefono,
			int codigoCarrera, String estado) {
		super();
		this.dni = dni;
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.codigoCarrera = codigoCarrera;
		this.estado = estado;
	}
	// Metodos Accesores
	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getCodigoCarrera() {
		return codigoCarrera;
	}

	public void setCodigoCarrera(int codigoCarrera) {
		this.codigoCarrera = codigoCarrera;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
