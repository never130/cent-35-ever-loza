
public class FooClase {
	void barMetodo() {
		// Note fooMetodo() y this.Metodo() son la misma cosa.
		String s1 = fooMetodo("Pato Donald");
		String s2 = this.fooMetodo("Condorito");

		System.out.println("s1 = " + s1 + " s2 = " + s2);
	}

	String fooMetodo(String nombre) {
		return "Hola " + nombre;
		
		//Error de compilacion - no podemos invocar un metodo de instancia
		// desde un metodo estatico
		
	}
}
