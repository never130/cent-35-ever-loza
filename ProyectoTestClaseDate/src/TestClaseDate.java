import java.text.SimpleDateFormat;
import java.util.Date;

public class TestClaseDate {

	public static void main(String[] args) {
		//Creamos e inicializamos el objecto Date de Java util
		Date d1 = new Date();
		
		//Imprimimos la Fecha
		System.out.println("Fecha: " + d1);
		
		//Damos formato a la fecha con SimpleDateFormat
		SimpleDateFormat sdf = new SimpleDateFormat("dd'de' MMMM,yyyy");
		System.out.println("Damos formato a la fecha: " + sdf.format(d1));
		
		// Con el m�todo getTime() obtenemos la fecha en milisegundos como un tipo long
		// Hacemos una pausa de tiempo con un for para obtener un lapso de tiempo
		for (int i = 0; i < 10000000; i++){
			int j = i;
		}
        
		Date d2 = new Date();
		
		long tiempoTranscurrido = d2.getTime() - d1.getTime();
		System.out.println("Tiempo Transcurrido en el for: " + tiempoTranscurrido + " milliseconds");
		
	}

}
