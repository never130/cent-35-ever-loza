

public class Funciones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("AND");
		System.out.println(" 0 | 0 " + compAnd ( false, false));
		System.out.println(" 0 | 1 " + compAnd ( false, true));
		System.out.println(" 1 | 0 " + compAnd ( true, false));
		System.out.println(" 1 | 1 " + compAnd ( true, true));

		System.out.println("OR");
		System.out.println(" 0 | 0 " + compOr ( false, false));
		System.out.println(" 0 | 1 " + compOr ( false, true));
		System.out.println(" 1 | 0 " + compOr ( true, false));
		System.out.println(" 1 | 1 " + compOr ( true, true));

		System.out.println("XOR");
		System.out.println(" 0 | 0 " + compXor ( false, false));
		System.out.println(" 0 | 1 " + compXor ( false, true));
		System.out.println(" 1 | 0 " + compXor ( true, false));
		System.out.println(" 1 | 1 " + compXor ( true, true));

	}
	//GENERACION DE CLASES PROPIAS.
	public static boolean compAnd(boolean a, boolean b) {
		boolean c = false;
		if (a && b)
			c = true;
		return c;
	}

	public static boolean compOr(boolean a, boolean b) {
		boolean c = false;
		if (a || b)
			c = true;
		return c;
	}

	public static boolean compXor(boolean a, boolean b) {
		boolean c = false;
		if (a || b)
			c = true;
		if (a && b)
			c = false;
		return c;
	}
}
