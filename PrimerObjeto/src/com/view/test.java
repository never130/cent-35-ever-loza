package com.view;

import java.util.ArrayList;
import com.domain.Persona;

public class test {

	public static void main(String[] args) {
		
    ArrayList<Persona> personas = new ArrayList<Persona>();
    //inicializo con un array, ya importado, para personas
    
    Persona p = new Persona ();
    //se importa el atributo Persona
    p.nombre="Ever";
    p.telf="123";
    p.email="ever@hotmail.com";
    
    personas.add(p);
    //se a�ade el valor de "p"
    
    Persona p2 = new Persona ();
    p2.nombre= "Ricky";
    p2.telf= "666";
    p2.email= "Fort@hotmail.com";
    p2.referido= p2;	
    //referido vendria a ser el nombre del objeto, en este caso el de Ricky
    
    for (int i = 0; i < personas.size(); i++) {
		System.out.println(personas.get(i).nombre);
		//get nos devuelve el nombre de cada persona segun la posicion (i=0)
		if (personas.get(i).referido != null) {		
			System.out.println("Referido: " +personas.get(i).referido.referido.nombre);
			
		}
	}
	}

}
