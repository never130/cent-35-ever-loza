
public class EjemploMath {

	public static void main(String[] args) {
		int x;
		double ran, y, z;
		float max;
		ran = Math.random();
		x = Math.abs(-123);
		y = Math.round(123.456);
		z = Math.pow(2, 4);
		max = Math.max((float) 1e10, (float) 3e9);
		/*
		 * Se imprime en consola los numeros obtenidos de las operaciones
		 * anteriores para comprobar los resultados de las aplicaciones de los
		 * metodos definidos de la clase math.
		 */
		System.out.println(ran);
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(max);
        
	}

}
