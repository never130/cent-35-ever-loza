
public class TestClaseMatematica {

	public static void main(String[] args) {
		System.out.println("Valor absoluto de -5: " + Math.abs(-5));
		System.out.println("Valor absoluto de 5: " + Math.abs(5));
		System.out.println("Numero aleatorio (como maximo 10): " + Math.random() *10);
		System.out.println("Maximo entre 3.5 y 1.2: " + Math.max(3.5, 1.2));
		System.out.println("Minimo entre 3.5 y 1.2: " + Math.min(3.5,  1.2));
		System.out.println("El tope o techo de 3.5: " + Math.ceil(3.5));
		System.out.println("Piso de 3.5: " + Math.floor(3.5));
		System.out.println("Exponencial 1 (E elevado a 1): " + Math.exp(1));
		System.out.println("Logaritmo 10: " + Math.log(10) );
		System.out.println("Numero 10 elevado a la potencia de 3: " + Math.pow(10, 3));
		System.out.println("Redondear el valor de pi: " + Math.round(Math.PI));
		System.out.println("Raid cuadrada de 5 = " + Math.sqrt(5));
		System.out.println("Convierte de radianes a grados: 10 radian = " + Math.toDegrees(10) + "degrees");
		System.out.println("sin(90): " + Math.sin(Math.toRadians(90)));
		

	}

}
