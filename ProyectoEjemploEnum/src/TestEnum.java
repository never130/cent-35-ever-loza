
public class TestEnum {
	public enum Suit {
		CLUBS, DIAMONDS, HEARTS, SPADES
	}

	public enum Grade {
		A, B, C, D, E, F, IMCOMPLETE
	}

	public static void main(String[] args) {
		Grade valorCarta;
		valorCarta = Grade.C;
		for (Suit suit : Suit.values()) {
			System.out.println(suit);
		}
		Grade[] gradeValues = Grade.values();
		for (int i = 0; i < gradeValues.length; i++) {
			System.out.println(gradeValues[i]);
		}
		

	}
}
